# Scenario: Acquisition of data 

## Steps 

* Onboard Company B as a Participant (“Data Provider”), then create VC for employee Bob
* Add Data Product offering to the federated Data Catalog. 
* Onboard Company A as a Participant (“Data Consumer”), then create VC for employee Alice
* Alice searches the Data Catalog for a Data Product based on criteria 
* Alice and Bob Bob negotiate and agree on Data Contract
* Alice get access to the Data Product

Assumption : company A and B are part of a same federation created for the scenario
Assumption : Company A, company B, Alice and Bob have a Wallet with a valid DID

## Schema 

### Part I, onboard  

Description:
* Bob is the administrator of Company B and want to identify Company B as a provider to the GXDCH 
Steps: 
* Bob retrieves Participant (“Data Provider”) schema description
* Bob fulfills the provider self description according to the schema
* Bob send the Self-description to the GXDCH Trust Framework API
* GXDCH Trust Framework validates the provider Self-Description request (checks the claims, run accreditation process) then returns a VC* 

assumption the wallet has all required VC like ISO27001
 
```mermaid
sequenceDiagram
actor Bob
participant WalletCompanyB
participant WalletBOB
participant notarization
participant compliance
participant registry 
Bob ->> registry: get schema description 
Bob ->> notarization: request legal participant as Data Provider
Bob ->> WalletCompanyB: add legal participant
Bob ->> notarization: request participant for Bob 
Bob ->> WalletBOB: add participant for Bob to the Wallet 
Bob ->> compliance: request Compliance for Company B with a SD (VP)
Bob ->> WalletCompanyB: add the Compliance to the Wallet 
```

### Part II Add Data Product offering to the federated Data Catalog. 

Description :
* Bob is the administrator of Company B declared as provider inside the GXDCH
* Bob want to expose a Data Product according to its terms of use

TODO define administrator. is it CEO of company B ?

Steps :
* Bob retrieves “Data Product" schema description 
* Bob fulfills a service Self-Description of the Data Product according to the schema (title, description, tags, data set, types, data policy, license, ...) 
* Bob send the Self-description to the GXDCH Trust Framework API
* Bob calls the GXDCH Trust Framework API to validate the Data Product Self-Description request.
* Bob register his Data Product offering to the federated Data Catalog

```mermaid
sequenceDiagram
actor Bob
participant WalletCompanyB
participant FederetedCatalogWizard
participant compliance 
Bob ->> FederetedCatalogWizard : create a data product and get a vc
Bob ->> WalletCompanyB: add the vc for the data product into to Wallet 
Bob ->> compliance: request compliance 
Bob ->> WalletCompanyB: add the compliance to the Wallet 
Bob ->> WalletCompanyB: request a VP for the service
Bob ->> FederetedCatalogWizard: add the service to the federated catalog 
```

### Part III Onboard Company A as a Participant (“Data Consumer”), then create VC for employee Alice

Description :
* Anna is the administrator of Company A and wants to on-board Company A as a federated service consumer, then create VC for Alice, a BI Analyst of Company A

__Pourrquoi Anna et pas Alice ? c est laice partout ailleurs__

Steps :
* Anna retrieves Participant (“Data Consumer”) schema description
* Anna fulfills the Participant (“Data Consumer”) Self-Description according to the schema
* Anna send the Self-description to the GXCDH Trust Framework API
* GXCDH  Trust Framework validates the participant (“Data Provider”) Self-Description request (checks the claims, run accreditation process) then returns a VC
* Anna creates a signed VC for Alice with specific attributes

assumption WalletCompany has all required VC like ISO27001

```mermaid
sequenceDiagram
actor Anna
participant WalletCompanyA
participant WalletAnna
participant WalletAlice 
participant notarization
participant registry
participant FederetedCatalogWizard
Anna ->> registry: get schema 
Anna ->> notarization: request legal participant
Anna ->> WalletCompanyA: add legal participant
Anna ->> FederetedCatalogWizard: create data service description
FederetedCatalogWizard ->> compliance: request compliance  
Anna ->> WalletCompanyA: add service description and compliance 
Anna ->> WalletCompanyA: create a VC Employee for Alice 
Anna ->> Alice: send VC Employee for Alice 
Alice ->> WalletAlice: add VC Employee 
Alice  ->> notarization: request particpant for Alice   
```

### Part IV Alice searches the Data Catalog for a Data Product based on criteria

Description :
* Alice is a BI analyst from Company A and wants to search a Data Product 
Steps :
* Alice connects to the federation Portal
* The federation Portal checks Alice credentials in regards to the authentication module and grants access
* Alice search Data Product in the federated Data Catalog based on the attributes of a Data Product self description (title, description, tags, data set, types, localization, terms & conditions, license, ...)
* Alice discovers the Data Product from Company B

```mermaid
sequenceDiagram
actor Alice
participant WebFederationCatalog
participant WalletAlice 
participant IAM
Alice ->>IAM:  request access (get a VP Request)
Alice ->> WalletAlice: provide the VP request to get a VP 
WalletAlice ->> IAM: send the VP signed 
Alice ->> WebFederationCatalog: search a data product using an authenticated session
```

### Part V Alice and Bob negotiate and agree on Data Contract

assumption : standard contract ? if automatic bob is not required 

Description :
* Alice of company A negotiates, agrees and signs a Data Contract with Bob of company B
Steps :
* Alice and Bob agree on terms via a Data Intermediary (Assumption: in the demo, the Federation Portal acts as a Data Intermediary)
* A Data Contract is digitally signed
* The Data Contract VC is sent by the Data Intermediary to the GXCDH Trust Framework API to validate the schema
* The Data Contract is notarized 

```mermaid
sequenceDiagram
actor Alice
actor Bob 
participant WebFederationCatalog
participant ProviderContract
Alice ->> WebFederationCatalog: request a standard contract url
Alice ->> ProviderContract: display a contract including some attachments to have VC ... and sign it 
Alice ->> ProviderContract: sign contract
Bob ->> ProviderContract: sign contract 
ProviderContract ->> Alice: send a vc with all links signed 
ProviderContract ->> Bob: send a vc with all links signed 
```
TODO update ontology to have specific fields to start a contract process from the web front (provider url to initiate a process)

### Part VI Alice get access to the Data Product


Description :
* Alice is a BI analyst from Company A and wants to access the Data Product 
Steps :
* Alice connects to the Federation Portal
* The Federation Portal checks Alice credentials in regards to Data Contract, checks for Data Consent (Nov. 2023) and grants access to the Data Product (Sept 2023)
* The Data Transaction is notarized by the Federation Portal 

Question : what is a data consent ? a VC ? 

```mermaid
sequenceDiagram
actor Alice
participant ProviderService 
participant FederationPortal 
participant WalletAlice 
Alice ->> FederationPortal: connect required
Alice ->> WalletAlice: provide the VP Request 
WalletAlice ->> Alice: Generate a VP 
Alice ->> FederationPortal: connect to the portal 
Alice ->> FederationPortal: access to a data product and check data consent
```
# Scenario:On-boarding company B as Data Provider, create VC for Bob

### Part VII chain of Trust

Description :
* Alice Demonstration of a Trusted Data Transaction 

Steps :
* Alice connects to the Federation Portal
* Alice get access to traces logged / notarized by the Federation Portal 

