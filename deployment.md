```mermaid
C4Deployment
    title Deployment Diagram for scenario 23.11

    Enterprise_Boundary(p_CA, "MontBlanc IOT"){
        Person(p_Alice,"Alice","BI Analyst of MontBlanc IOT")
        Person(p_Mary,"Mary","Admin of Mont Blanc IOT")
        System_Boundary(ns_MB,"Namespace: provider montblanc-iot"){
            Container(ca_VCI, "vc-issuer", "MontBlanc VC Issuer")
            Container(ca_DID, "DID <br> https://montblanc-iot.gaia-x.community/.well-known/did.json")
            Container(ca_UAc, "participant-agent", "Participant Agent API")
            Container(ca_WW, "Web Wallet", "Alice Web Wallet")
        }   
    }

    Enterprise_Boundary(p_CB, "Dufour Storage"){
        Person(p_B,"Bob","VP Marketing Dufour Storage")
        System_Boundary(ns_B,"Namespace: Provider Dufour"){
            Container(cb_VCI, "vc-issuer", "Dufour Storage VC Issuer")
            Container(cb_DID, "DID <br> https://dufourstorage.gaia-x.community/.well-known/did.json")
            Container(cb_UAc, "participant-agent", "Participant Agent API")
            Container(cb_sdui,"service-description-wizard-tool")
            Container(cb_Storage, "Minio Service")
            Container(cb_Bridge, "OpenId Bridge")
            Container(cb_PRC, "OPA")
        }
    }

    Enterprise_Boundary(p_GX, "Gaia-X"){
        System_Boundary(ns_GX,"Namespace: Gaia-X"){
            Container(gx_VCI, "vc-issuer", "Gaia-X VC Issuer")
            Container(gx_DID, "DID <br> https://gaia-x.community/.well-known/did.json")
        }
        System_Boundary(ns_GXLAB,"Namespace: Gaia-X (LAB)"){
            Container(gxl_REG, "lab registry", "Registry Service")
            Container(gxl_COM, "lab compliance", "Compliance Service")
            Container(gxl_NOT, "lab notary", "Notary Service")
            Container(gxl_DID, "DID <br> ??")
        }

    }

    Enterprise_Boundary(p_ABC, "Federator - ABC FEDERATION"){
        System_Boundary(ns_ABC,"Namespace: abc-federation"){
            Container(abc_FAc, "participant-agent", "Participant Agent API")
            Container(abc_DID, "DID <br> https://abc-federation.gaia-x.community/.well-known/did.json")
            Container(abc_VCIc, "vc-issuer", "VC Issuer")
            Container(abc_UAc, "participant-agent", "Participant Agent API")
            Container(abc_sn,"notarization","Notarization")
            Container(abc_cfsc, "wizard-federated-catalog", "Wizard Federated Catalogue")
            Container(abc_cfsc, "federated-catalog-api", "Federated Service Catalogue")
            Container(abc_REG, "lab registry", "Registry Service")
            Container(abc_COM, "lab compliance", "Compliance Service")
            Container(abc_NOT, "lab notary", "Notary Service")
            Container(abc_LAB, "labelling", "Labelling Service")
        }
    }

    Enterprise_Boundary(p_CAB, "CAB <unknown>"){
        System_Boundary(ns_CAB,"Namespace: <unknown>"){
            Container(cab_PA, "participant-agent", "Participant Agent API")
            Container(cab_DID, "DID <br> https://unknown.auditor.gaia-x.community/.well-known/did.json")
            Container(cab_VCI, "vc-issuer", "VC Issuer", "")
        }
    }


UpdateLayoutConfig($c4ShapeInRow="4", $c4BoundaryInRow="2")

```
